<?php

namespace App\Http\Controllers;

use App\Country;
use App\CustomClasses\ChunkReadFilter;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;


class UsersController extends Controller
{

    public function index()
    {
        $users = User::with("country")->paginate(15);

        return view('users.index', compact('users'));
    }
}





