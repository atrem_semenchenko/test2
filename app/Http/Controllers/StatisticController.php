<?php

namespace App\Http\Controllers;

use App\Country;
use App\CustomClasses\ChunkReadFilter;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;


class StatisticController extends Controller
{

    public function index()
    {
        $statistic = [
            'count_users'   => User::all()->count(),
            'count_m'       => User::where('gender', 1)->count(),
            'count_f'       => User::where('gender', 0)->count(),
            'average_age'   => User::all()->avg('age'),
            'average_age_m' => User::where('gender', 1)->avg('age'),
            'average_age_f' => User::where('gender', 0)->avg('age'),
            'countries'     => Country::withCount('usersCount')->get()
        ];

        return view('statistic.index', compact("statistic"));
    }
}





