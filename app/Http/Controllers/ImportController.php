<?php

namespace App\Http\Controllers;

use App\Country;
use App\CustomClasses\ChunkReadFilter;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;


class ImportController extends Controller
{
    private $usersData = [];

    public function index()
    {
        return view('import.index');
    }

    public function import(Request $request)
    {
        $mem_start = memory_get_usage();
        set_time_limit(1800);

        $mimes = ['xlsx', 'xls', 'docx', 'ppt', 'odt', 'ods', 'odp'];
        if (!$request->file('file')) {
            return back()->with('error', 'You need to upload file');
        } elseif(!in_array(strtolower($request->file('file')->getClientOriginalExtension()), $mimes)) {
            return back()->with('error', 'Wrong file type');
        }

        $filePath = $request->file('file')->getRealPath();
        $chunkSize = 2000;
        $startRow = 2;
        $stop = false;
        $countColumns = 6;

        $objReader = new Xlsx();
        $objReader->setReadDataOnly(true);

        $chunkFilter = new ChunkReadFilter();
        $objReader->setReadFilter($chunkFilter);
        while (!$stop ) {
            $chunkFilter->setRows($startRow, $chunkSize);
            $objPHPExcel = $objReader->load($filePath);
            $objPHPExcel->setActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            for ($i = $startRow; $i < $startRow + $chunkSize; $i++) {
                if ($stop) continue;
                for ($col = 1; $col <= $countColumns; ++$col) {
                    $value = $objWorksheet->getCellByColumnAndRow($col, $i)->getValue();
                    if (!$value) {
                        $stop = true;
                        continue;
                    }
                    $this->usersData[$i][] = $value;
                }
            }

            $objPHPExcel->disconnectWorksheets();
            unset($objPHPExcel);
            $startRow += $chunkSize;
        }

        if ($this->usersData) {
            foreach ($this->usersData as $user) {
                $gender = strtolower($user[2]) == 'male' ? 1 : 0;
                $country = Country::firstOrCreate(['name' => $user[3]]);
                $created = \DateTime::createFromFormat("d/m/Y", $user[5]);
                User::create([
                    'firstname' => $user[0],
                    'lastname' => $user[1],
                    'gender' => $gender,
                    'country_id' => $country->id,
                    'age' => $user[4],
                    'created' => $created,
                ]);
            }
        }

        return back()->with('success', 'Excel Data Imported successfully. Memory used: '. (memory_get_usage() - $mem_start));
    }
}





