<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public $timestamps = false;

    protected $fillable = ['name'];

    public function usersCount()
    {
        return $this->hasMany('App\User', 'country_id', 'id');
    }
}
