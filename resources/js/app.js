require('./bootstrap');

$(document).ready(function() {

    $('.cross').on("click", function() {
        let blockText = $('.inputFile');
        blockText.text("Add Your File").parent("p").removeClass("active");
        $('#file')[0].value = ""
    });

    function uploadFile() {
        let typeInput = $('input[type="file"]');

        typeInput.change(function(){
            let value = typeInput.val();
            let blockText = $('.inputFile');

            if (value) {
                let nameFile = value.split("\\").pop();
                blockText.text(nameFile).parent("p").addClass("active");
            } else {
                blockText.text("Add Your File").parent("p").removeClass("active");
            }
        });
    }

    $('input[type="radio"]').on("click", function() {
        if (this.id === "corporation" || this.id === "other") {
            $('#hide-block').fadeIn().find('.label').text($(this).data('label'));
        } else {
            $('#hide-block').fadeOut()
        }
    });

    uploadFile()
});