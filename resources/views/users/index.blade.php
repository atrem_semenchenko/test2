@extends('layouts.app')

@section('header')
    @include('layouts.components.header')
@endsection

@section('content')
    <div class="container page-title">
        <div class="row">
            <div class="col-md-8">
                <h2>Users list</h2>
            </div>
            <div class="col-md-2">
                <a href="{{ url('/') }}" class="btn btn-primary">Back to Homepage</a>
            </div>
        </div>
    </div>

    <div class="container users-list-table">
        @if (count($users))
            @foreach ($users as $user)
                @if ($loop->first)
                    <div class="row user-list-header">
                        <div class="col-md-1 my-auto">ID</div>
                        <div class="col-md-2 my-auto">First name</div>
                        <div class="col-md-2 my-auto">Last name</div>
                        <div class="col-md-2 my-auto">Gender</div>
                        <div class="col-md-2 my-auto">Country</div>
                        <div class="col-md-1 my-auto">Age</div>
                        <div class="col-md-2 my-auto">Created</div>
                    </div>
                @endif
                    <div class="row user-list">
                        <div class="col-md-1 my-auto">{{ $user->id }}</div>
                        <div class="col-md-2 my-auto">{{ $user->firstname }}</div>
                        <div class="col-md-2 my-auto">{{ $user->lastname }}</div>
                        <div class="col-md-2 my-auto">{{ $user->gender ? "Male" : "Famale" }}</div>
                        <div class="col-md-2 my-auto">{{ $user->country->name }}</div>
                        <div class="col-md-1 my-auto">{{ $user->age }}</div>
                        <div class="col-md-2 my-auto">{{ $user->created }}</div>
                    </div>
            @endforeach
        @else
            <h2>There is no users in database</h2>
        @endif
    </div>

    @if (count($users))
        <div class="container users-list-footer">
            {{ $users->links() }}
        </div>
    @endif
@endsection