@extends('layouts.app')

@section('header')
    @include('layouts.components.header')
@endsection

@section('content')
    <div class="container page-title">
        <div class="row">
            <div class="col-md-8">
                <h2>Statistics by users</h2>
            </div>
            <div class="col-md-2">
                <a href="{{ url('/') }}" class="btn btn-primary">Back to Homepage</a>
            </div>
        </div>
    </div>

    <div class="container users-list-table">
        @if ($statistic)
            <div class="row">
                <div class="col-md-10">
                    <h3>Количество пользователей</h3>
                </div>
                <div class="col-md-2">
                    <h3>{{ $statistic['count_users'] }}</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <h3>Количество женщин</h3>
                </div>
                <div class="col-md-2">
                    <h3>{{ $statistic['count_f'] }}</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <h3>Количество мужчин</h3>
                </div>
                <div class="col-md-2">
                    <h3>{{ $statistic['count_m'] }}</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <h3>Средний возраст всех сотрудников</h3>
                </div>
                <div class="col-md-2">
                    <h3>{{ number_format($statistic['average_age'], 2) }}</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <h3>Средний возраст мужчин</h3>
                </div>
                <div class="col-md-2">
                    <h3>{{ number_format($statistic['average_age_m'], 2) }}</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <h3>Средний возраст женщин</h3>
                </div>
                <div class="col-md-2">
                    <h3>{{ number_format($statistic['average_age_f'], 2) }}</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h3>Количество пользователей из каждой страны:</h3>
                </div>
            </div>
            @if($statistic['countries'])
                @foreach($statistic['countries'] as $country)
                    <div class="row">
                        <div class="col-md-9 offset-md-1">
                            <h3>- {{ $country->name }}</h3>
                        </div>
                        <div class="col-md-2">
                            <h3>{{ $country->users_count_count }}</h3>
                        </div>
                    </div>

                @endforeach
            @endif
        @else
            <h2>There is no users in database</h2>
        @endif
    </div>
@endsection