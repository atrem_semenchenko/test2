<form action="{{ route('import') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group row">
                    <label for="file">
                        <h3>Upload your file</h3>
                        <p class="file-block">
                            <span class="inputFile">Add Your File</span>
                            <button class="cross" type="button">&times;</button>
                        </p>
                    </label>
                    <input class="hide" id="file" type="file" name="file"/>
                </div>
                <button class="btn btn-primary" type="submit">Upload</button>
            </div>
        </div>
    </div>
</form>